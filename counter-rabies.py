from sys import argv
import math
import numpy as np
import os.path as opath
import pickle

from cw_components.cells3d import Cells3D
from cw_components.atlas import AtlasReader, AtlasPoint

# Cell Count Type: Rabies
# Type 1: Cortical Rabies Cells
# Type 2: Rest of Brain Rabies Cells (including contralateral cortex)
# Type 3: TVA cells
# Type 4: Cortical Curvature Landmarks (1-midline level with cortex, 2-point along upper
#		  cortex, 3- top of rhinal sulcus, 4-middle of rhinal sulcus, 5-bottom of rhinal
#		  sulcus, 6-bottom of cortex)
# Type 5: Top of midline, bottom of midline
# Type 6: 1-Center of rhinal sulcus, 2- point along straight line at white matter
# Type 7: Top of injection track, bottom of injection track (only in sections w injection)
# Note: Marker Z =1 for Rabies/Injection Site, Z=2 for TVA, Z=3 for landmarks

class CorticalRabies:
	def __init__(self, datadir, atlasdir='/Users/arthur/Documents/Programs/CellWarp/atlas/', save=False):
		self.atlasdir = atlasdir
		if save: self.drawCorrections = datadir
		c3d = Cells3D(datadir)
		self.name = opath.split(datadir)[1][:4].lower()
		self.zs = c3d.zs()
		self.corrected = [False for z in self.zs]
		self.correctionchecked = [False for z in self.zs]
		self.cells = c3d.get()
		self.atlas = AtlasReader(atlasdir)

		self._recover(datadir)
		self._analyze(datadir)

	# Get a list of coordinates for selector dict.
	def coords(self, selectors, flats=False):
		if 'z' not in selectors:
			for z in self.zs:
				self._correct_z(z)
		else:
			selectors['z'] = self._get_z(selectors['z'])
			self._correct_z(selectors['z'])

		out = self._select(selectors, corrected=True)
		if not flats:
			return [(c['x'], c['y'], c['z']) for c in out]
		else:
			return [(c['x'], c['y'], c['z']) for c in out], [c['flat'] for c in out]

	def get(self, selectors={}):
		if 'z' not in selectors:
			for z in self.zs:
				self._correct_z(z)
		else:
			selectors['z'] = self._get_z(selectors['z'])
			self._correct_z(selectors['z'])

		cells = self._select(selectors, corrected=True)
		return [{'mediolateral': c['x'], 'dorsoventral': c['y'], 'anteroposterior': c['z'], 'pial': c['flat'][1], 'flattened': c['flat'][0]} for c in cells]

	def _save(self, path):
		print 'SAVING...'
		fp = open(opath.join(path, 'pickled-cells.txt'), 'w')
		pickle.dump({'corrected': self.corrected, 'correctionchecked': self.correctionchecked, 'cells': self.cells}, fp)
		fp.close()

	def _recover(self, path):
		if opath.exists(opath.join(path, 'pickled-cells.txt')):
			with open(opath.join(path, 'pickled-cells.txt')) as fp:
				out = pickle.load(fp)
				self.corrected = out['corrected']
				self.correctionchecked = out['correctionchecked']
				self.cells = out['cells']

	def _analyze(self, path):
		tosave = False
		for z in sorted(self.zs):
			if not self.correctionchecked[self.zs.index(z)]:
				self._correct_z(z)
				if self.correctionchecked[self.zs.index(z)]:
					tosave = True
		if tosave:
			self._save(path)

	def rabiescells(self):
		return sorted(self.get({'marker':1}), key=lambda c:c['anteroposterior'])

	def rhinalsulcus(self):
		out = []
		for z in sorted(self.zs):
			sulcus = sorted(self.get({'marker':6, 'z':z}), key=lambda c: c['mediolateral'])
			if len(sulcus) > 0:
				out.append(sulcus[0])
		return out

	def injection(self):
		out = {}
		for z in sorted(self.zs):
			self._correct_z(z)
			inj = self.get({'marker':7, 'z':z})
			for i in inj:
				for key in i:
					if key not in out:
						out[key] = [i[key]]
					else:
						out[key].append(i[key])

		for key in out:
			out[key] = float(np.mean(out[key]))

		if out == {}:
			print '\tInjection coordinates not found via beads-- guessing...'
			# mediolateral, anteroposterior, depth
			# X, Z, Y
			injAim = {'k137': [-4.6, -4.3, 0.4], 'k139': [-4.0, -4.3, 0.4], 'k143': [-4.6, -4.3, 0.4], 'k178': [-4.2, -4.3, 3.8]}
			injCoords = injAim[self.name]
			z = self._get_z(injCoords[1])

			markers = self.atlas.get(z)
			corticalEdge = markers['cyan'][0]
			rh = markers['yellow'][1].xy()
			rhf = corticalEdge.flatxy(rh)

			ycorr = -rhf[0] + rh[1]

			flatp = corticalEdge.flatxy((injCoords[0], injCoords[2]))

			out = {'mediolateral': injCoords[0], 'dorsoventral': injCoords[2], 'anteroposterior': injCoords[1], 'pial': flatp[1], 'flattened': flatp[0]}

		return out

	def slicewidth(self):
		szs = np.array(sorted(self.zs), dtype=np.float32)
		return np.median(szs[1:] - szs[:-1])

	# Draw the corrections on a coronal slice
	def test(self, z):
		z = self._get_z(z)
		self._correct_z(z)

		ps = [(c['x'], c['y']) for c in self._select({'marker':1, 'z':z})]
		self.atlas.mark(z, ps, 'indigo')
		flatline = self.atlas.get(z)['cyan'][0]
		flatps = [flatline.flatxy(p) for p in ps]
		flatps = [(p[0] - 4.5, p[1] - 1.0) for p in flatps]
		self.atlas.mark(z, flatps, 'indigo')
		self.atlas.line(z, (-4.5, -1.0), (flatline.length() -4.5, -1.0), 'cyan')
		print 'Flattened cortex is of %.2f mm long' % (flatline.length())

		ps = [(c['x'], c['y']) for c in self._select({'marker':4, 'z':z})]
		self.atlas.mark(z, ps, 'mint')
		flatps = [flatline.flatxy(p) for p in ps]
		flatps = [(p[0] - 4.5, p[1] - 1.0) for p in flatps]
		self.atlas.mark(z, flatps, 'mint')

		ps = [(c['x'], c['y']) for c in self._select({'marker':5, 'z':z})]
		self.atlas.mark(z, ps, 'orange')

		ps = [(c['x'], c['y']) for c in self._select({'marker':6, 'z':z})]
		self.atlas.mark(z, ps, 'red')
		flatps = [flatline.flatxy(p) for p in ps]
		flatps = [(p[0] - 4.5, p[1] - 1.0) for p in flatps]
		self.atlas.mark(z, flatps, 'red')

		self.atlas.write(z, 'temp2.svg')

	def draw_gradientfield(self, z, spacing=0.5, xrange=(-5, 0), yrange=(0, 6)):
		z = self._get_z(z)
		self._correct_z(z, gradcorrect=False)

		ps = [(c['x'], c['y']) for c in self._select({'marker':1, 'z':z})]
		self.atlas.mark(z, ps, 'indigo', ring=True)
		ps = [(c['x'], c['y']) for c in self._select({'marker':4, 'z':z})]
		self.atlas.mark(z, ps, 'mint', ring=True)

		# Run gradient vector field correction
		vecs = self._generate_gradient_field(z)

		for x in np.arange(xrange[0], xrange[1] + spacing, spacing):
			for y in np.arange(yrange[0], yrange[1] + spacing, spacing):
				old = (float(x), float(y))
				new = self._gradientfield(old, vecs)
				self.atlas.arrow(z, old, new, 'gray')

		self._gradientcorrection(z, vecs)
		ps = [(c['x'], c['y']) for c in self._select({'marker':1, 'z':z})]
		self.atlas.mark(z, ps, 'indigo')
		ps = [(c['x'], c['y']) for c in self._select({'marker':4, 'z':z})]
		self.atlas.mark(z, ps, 'mint')

		self.atlas.write(z, 'gradientfield.svg')

	def flatmap(self):
		for z in sorted(self.zs):
			print '\tAnalyzing z %.2f... ' % (z),
			corticalEdge = self.atlas.get(z)['cyan'][0]

			cells = self.coords({'marker':1, 'z':z})
			sulcus = sorted(self.coords({'marker':6, 'z':z}), key=lambda tup: tup[0])

			print '%i cells, sulcus is %smarked--' % (len(cells), '' if len(sulcus) > 0 else 'not '),

			injsite = self.coords({'marker':7, 'z':z})

			if len(cells) > 0 and len(sulcus) > 0:
				print 'included'
				sulcus = sulcus[0]
				fsulcus = corticalEdge.flatxy(sulcus)
				fcells = [corticalEdge.flatxy(p) for p in cells]

				ycorr = -fsulcus[0] + sulcus[1]
				pcells = [(cells[i][2], fcells[i][0] + ycorr) for i in range(len(cells))]

				self.atlas.mark(-3.52, pcells, 'indigo')
				self.atlas.mark(-3.52, [(sulcus[2], fsulcus[0] + ycorr)], 'red')
			else:
				print 'excluded'

		self.atlas.write(-3.52, 'flatmap.svg')



	# Correct based on required coordinate corrections, then do matching to lines
	def _generate_gradient_field(self, z):
		midline = self._select({'z':z, 'marker':5})
		midline = sorted([(m['x'], m['y']) for m in midline], key=lambda tup: tup[1])

		# Get the edge points sorted so the top is index 0
		edge = self._select({'z':z, 'marker':4})
		edge = sorted([(m['x'], m['y']) for m in edge], key=lambda tup: tup[1])

		# Get the atlas marker points, separated by color
		markers = self.atlas.get(z)
		rhinalSulcus = markers['yellow'][1]
		cortexTop = markers['green'][0]
		cortexBot = markers['green'][1]

		# First list of vectors, which we will use to generate second round corrections
		vecs = [midline[0] + midline[0], midline[1] + midline[1], cortexTop.xy(edge[0]), rhinalSulcus.xy(edge[3]), cortexBot.xy(edge[-1])]

		# Correct the point along the dorsal cortex to lie along the cyan line
		cortexDorsal = markers['cyan'][0]
		newp = self._gradientfield(edge[1], vecs)
		cortexDorsalLine = cortexDorsal.xy(newp)

		vecs.append((edge[1][0], edge[1][1], cortexDorsalLine[2], cortexDorsalLine[3]))

		# Correct the white matter
		sulcus = self._select({'z':z, 'marker':6})
		sulcus = sorted([(m['x'], m['y']) for m in sulcus], key=lambda tup: tup[0])

		whiteMatter = markers['orange'][0]
		newp = self._gradientfield(sulcus[1], vecs)
		whiteMatterLine = whiteMatter.xy(newp)

		vecs.append((sulcus[1][0], sulcus[1][1], whiteMatterLine[2], whiteMatterLine[3]))

		return vecs

	# Correct the z coordinates. This is the key function of the class
	def _correct_z(self, z, gradcorrect=True):
		if not self.correctionchecked[self.zs.index(z)]:
			print '\tCorrecting z %.2f' % (z)

			self.atlas = AtlasReader(self.atlasdir)

			# Straighten out the slice
			midline = sorted([(c['x'], c['y']) for c in self._select({'z': z, 'marker': 5})], key=lambda c: c[1])
			self._verticalize(z, midline[0], midline[1])

			# Fix the coordinates relative to bregma
			markers = self.atlas.get(z)
			atlasMidOffset = markers['green'][0].xy()[1] - markers['red'][0].xy()[1]
			sliceMidPosition = sorted([(c['x'], c['y']) for c in self._select({'z': z, 'marker': 4})], key=lambda c: c[1])[0]
			self._translate(z, -1*sliceMidPosition[0], -1*sliceMidPosition[1] + atlasMidOffset)

			sulcus = sorted(self._select({'marker':6, 'z':z}), key=lambda c: c['x'])
			if sulcus[0]['x'] > 0:
				print 'Flipping...'
				self._flip_mediolateral(z)

			if len(self.drawCorrections) > 0: self.draw_initial_positions(z)

			if 'yellow' in markers or not gradcorrect:
				if gradcorrect:
					# Run gradient vector field correction
					vecs = self._generate_gradient_field(z)
					self._gradientcorrection(z, vecs)

				# Mark the slice as corrected
				self.corrected[self.zs.index(z)] = True

				corticalEdge = self.atlas.get(z)['cyan'][0]
				rh = markers['yellow'][1].xy()
				rhf = corticalEdge.flatxy(rh)

				ycorr = -rhf[0] + rh[1]
				zcellis, zcells = self._select({'z': z}, indices=True)
				for i, c in enumerate(zcells):
					flatp = corticalEdge.flatxy((c['x'], c['y']))
					flatp = (flatp[0] + ycorr, flatp[1])
					self.cells[zcellis[i]]['flat'] = flatp

			self.correctionchecked[self.zs.index(z)] = True
			if len(self.drawCorrections) > 0: self.draw_final_positions(z, self.drawCorrections)

	def draw_initial_positions(self, z):
		ps = [(c['x'], c['y']) for c in self._select({'marker':1, 'z':z})]
		self.atlas.mark(z, ps, 'indigo', ring=True)

		ps = [(c['x'], c['y']) for c in self._select({'marker':4, 'z':z})]
		self.atlas.mark(z, ps, 'mint', ring=True)

		ps = [(c['x'], c['y']) for c in self._select({'marker':6, 'z':z})]
		self.atlas.mark(z, ps, 'red', ring=True)

	def draw_final_positions(self, z, dir):
		ps = [(c['x'], c['y']) for c in self._select({'marker':1, 'z':z})]
		self.atlas.mark(z, ps, 'indigo')

		ps = [(c['x'], c['y']) for c in self._select({'marker':4, 'z':z})]
		self.atlas.mark(z, ps, 'mint')

		ps = [(c['x'], c['y']) for c in self._select({'marker':6, 'z':z})]
		self.atlas.mark(z, ps, 'red')

		self.atlas.write(z, opath.join(dir, 'test-%.2f.svg' % (z)))
		self.atlas.clear(z)

	# Draw the corrections on a coronal slice
	def draw_corrections(self, path):
		z = self._get_z(z)
		self._correct_z(z)

		ps = [(c['x'], c['y']) for c in self._select({'marker':1, 'z':z})]
		self.atlas.mark(z, ps, 'indigo')
		flatline = self.atlas.get(z)['cyan'][0]
		flatps = [flatline.flatxy(p) for p in ps]
		flatps = [(p[0] - 4.5, p[1] - 1.0) for p in flatps]
		self.atlas.mark(z, flatps, 'indigo')
		self.atlas.line(z, (-4.5, -1.0), (flatline.length() -4.5, -1.0), 'cyan')
		print 'Flattened cortex is of %.2f mm long' % (flatline.length())

		ps = [(c['x'], c['y']) for c in self._select({'marker':4, 'z':z})]
		self.atlas.mark(z, ps, 'mint')
		flatps = [flatline.flatxy(p) for p in ps]
		flatps = [(p[0] - 4.5, p[1] - 1.0) for p in flatps]
		self.atlas.mark(z, flatps, 'mint')

		ps = [(c['x'], c['y']) for c in self._select({'marker':5, 'z':z})]
		self.atlas.mark(z, ps, 'orange')

		ps = [(c['x'], c['y']) for c in self._select({'marker':6, 'z':z})]
		self.atlas.mark(z, ps, 'red')
		flatps = [flatline.flatxy(p) for p in ps]
		flatps = [(p[0] - 4.5, p[1] - 1.0) for p in flatps]
		self.atlas.mark(z, flatps, 'red')

		self.atlas.write(z, 'temp2.svg')

	def _dist(self, p1, p2): return math.sqrt((p2[0] - p1[0])*(p2[0] - p1[0]) + (p2[1] - p1[1])*(p2[1] - p1[1]))

	def _gradientcorrection(self, z, vecs, step=1.0, tolerance=0.00001, min=1000, max=10000):
		ps, cells = self._select({'z': z}, cells=True, indices=True)
		for i, cell in enumerate(cells):
			pnew = self._gradientfield((cell['x'], cell['y']), vecs, step, tolerance, min, max)
			self.cells[ps[i]]['x'] = pnew[0]
			self.cells[ps[i]]['y'] = pnew[1]

	# Gradient field generates a correction vector combining a series of input correction
	# vectors
	def _gradientfield(self, p1, vecs, step=1.0, tolerance=0.00001, min=1000, max=10000, squared=False):
		ds = np.array([self._dist(p1, (v[0], v[1])) for v in vecs], dtype=np.float64)
		weights = 1.0 - ds/np.sum(ds)
		if squared: weights = weights*weights
		weightsum = np.sum(weights)
		j = 0
		while j < max and (j < min or weightsum > tolerance):
			weights = weights - (weightsum - 1.0)*step*(1.0 - weights)*weights
			weightsum = np.sum(weights)
			j += 1

		x = np.sum(weights*np.array([v[2] - v[0] for v in vecs]))
		y = np.sum(weights*np.array([v[3] - v[1] for v in vecs]))
		return (p1[0] + x, p1[1] + y)

	# Rotates to make vertical around point p1
	def _verticalize(self, z, p1, p2):
		p3 = (p1[0], p2[1])
		d12 = math.sqrt((p2[0] - p1[0])*(p2[0] - p1[0]) + (p2[1] - p1[1])*(p2[1] - p1[1]))
		d13 = math.sqrt((p3[0] - p1[0])*(p3[0] - p1[0]) + (p3[1] - p1[1])*(p3[1] - p1[1]))
		d23 = math.sqrt((p2[0] - p3[0])*(p2[0] - p3[0]) + (p2[1] - p3[1])*(p2[1] - p3[1]))
		theta = math.acos((d12*d12 + d13*d13 - d23*d23)/(2*d12*d13))

		ps, cells = self._select({'z':z}, cells=True, indices=True)
		cells = [(c['x'], c['y']) for c in cells]
		for i, c in enumerate(cells):
			pcell = self._rotate_around_point(p1, c, -theta)
			self.cells[ps[i]]['x'] = pcell[0]
			self.cells[ps[i]]['y'] = pcell[1]

		return theta

	def _translate(self, z, xchange=0, ychange=0):
		ps = self._select({'z':z}, cells=False, indices=True)
		for i in range(len(ps)):
			self.cells[ps[i]]['x'] += xchange
			self.cells[ps[i]]['y'] += ychange

	def _flip_mediolateral(self, z):
		ps = self._select({'z':z}, cells=False, indices=True)
		for i in range(len(ps)):
			self.cells[ps[i]]['x'] = -self.cells[ps[i]]['x']

	def _rotate_around_point(self, c, p1, theta):
		x = math.cos(theta)*(p1[0] - c[0]) - math.sin(theta)*(p1[1] - c[1]) + c[0]
		y = math.sin(theta)*(p1[0] - c[0]) + math.cos(theta)*(p1[1] - c[1]) + c[1]
		return (x, y) if len(p1) == 2 else [x, y, p1[2]]

	# Function to index cells by z and return appropriately, taking into account problems
	# with floats
	def _select(self, selectors={}, cells=True, indices=False, corrected=False):
		sels = [key for key in selectors]
		outis = []
		outps = []
		for i, cell in enumerate(self.cells):
			fnd = True
			for key in sels:
				if key not in cell or cell[key] != selectors[key]:
					fnd = False
			if fnd:
				if not corrected:
					outps.append(cell)
					outis.append(i)
				else:
					z = self.zs.index(cell['z'])
					if self.corrected[z]:
						outps.append(cell)
						outis.append(i)

		if cells and indices: return (outis, outps)
		elif not cells and indices: return outis
		else: return outps

	# Return the closest anterior-posterior position from the atlas
	def _get_z(self, z):
		z = float(z)
		ds = [abs(z - key) for key in self.zs]
		return self.zs[ds.index(min(ds))]

class HeatMapper:
	def __init__(self, atlasdir='/Users/arthur/Documents/Programs/CellWarp/atlas/', test=False):
		self.atlas = AtlasReader(atlasdir)
		self.save_tests = test

		analyze = ['k139-bla-lm', 'k137-bla-por', 'k143-bla-por', 'k146-bla-lent', 'k147-bla-lent', 'k178-bla-lent']
		#analyze = ['k139-bla-lm', 'k137-bla-por', 'k143-bla-por', 'k146-bla-lent', 'k147-bla-lent']
		#analyze = ['k137-bla-por']
		#analyze = ['aav6-k79-por']
		self.d = []
		for a in analyze:
			print 'Analyzing %s...' % (a)
			self.read(a)

	def read(self, path):
		cr = CorticalRabies(path, save=self.save_tests)
		rcs = cr.rabiescells()
		inj = cr.injection()
		sul = cr.rhinalsulcus()
		swd = cr.slicewidth()
		if inj == {}:
			print '\tERROR: Injection site not found for %s' % (path)
		else:
			print '\tInjection at AP %.2f, Flat %.2f, From sulcus %.2f' % (inj['anteroposterior'], inj['flattened'], self._dist_sulcus(inj, sul))
			self.d.append({'name':path, 'rabies':rcs, 'injection':inj, 'sulcus':sul, 'slice-width':swd})

	def _dist_sulcus(self, inj, sulcus):
		zs = [i['anteroposterior'] for i in sulcus]
		ds = [abs(inj['anteroposterior'] - z) for z in zs]
		mz = ds.index(min(ds))
		return inj['flattened'] - sulcus[mz]['flattened']

	def _injection_ranges(self):
		if not hasattr(self, '_injection_ap'):
			self._injection_ap = [] # Injection extremes
			self._injection_flat = []

			for br in self.d:
				self._injection_flat.append(self._dist_sulcus(br['injection'], br['sulcus']))
				self._injection_ap.append(br['injection']['anteroposterior'])

		return (self._injection_ap, self._injection_flat)

	def draw_cells(self, path='flatmap-cells.svg'):
		for br in self.d:
			clr = self._color(br['injection'], br['sulcus'])
			self.atlas.mark(-9.99, [(cell['anteroposterior'], cell['flattened']) for cell in br['rabies']], clr)
			self.atlas.mark(-9.99, [(z['anteroposterior'], z['flattened']) for z in br['sulcus']], 'red')
			self.atlas.mark(-9.99, [(br['injection']['anteroposterior'], br['injection']['flattened'])], 'orange')

		self.atlas.group(-9.99)

	def render(self, path='flatmap-combined.svg'):
		self.atlas.write(-9.99, path)

	def integerize(self, brain, leg, minz=-99):
		minz -= 0.0001
		zs = [c['anteroposterior'] for c in brain['rabies'] if c['anteroposterior'] > minz]
		zs.extend([c['anteroposterior'] for c in brain['sulcus'] if c['anteroposterior'] > minz])
		zs = list(sorted(list(set(zs))))
		npzs = np.array(zs)
		ds = np.floor((npzs[1:] - npzs[:-1])/leg)
		while len(np.where(ds < 1)[0]) > 0:
			for i in range(len(ds) - 1):
				if ds[i] < 1:
					ds[i] = 1
					ds[i + 1] -= 1
			if ds[-1] < 1:
				ds[-1] = 1

		ds[np.where(ds < 1)] = 1
		ds = np.concatenate([np.zeros(1), ds])

		return (zs[0], {z: int(np.sum(ds[:list(zs).index(z)])) for z in zs})

	def draw_bounding_polys(self, path='flatmap-polys.svg', levels=[95.0, 75.0, 50.0, 25.0], amygdala=False):

		for br in self.d:
			if not amygdala:
				ps = []
				for cell in br['rabies']:
					if cell['anteroposterior'] < -2.3 or cell['flattened'] < 4.0 or cell['flattened'] > 5.2:
						ps.append((cell['anteroposterior'], cell['flattened']))
			else:
				ps = [(cell['anteroposterior'], cell['flattened']) for cell in br['rabies']]
			clr = self._color(br['injection'], br['sulcus'])

			opacs = [(i + 1)*(1.0/(len(levels) + 1)) for i in range(len(levels))]

			n = len(ps)
			allps = [p for p in ps]
			for i, level in enumerate(levels):
				while (len(ps) - 1.0)/n > level/100.0:
					ps = self.subtract_furthest_from_com(ps)

				bpoly = self.bounding_poly(ps)
				self.atlas.polygon(-9.99, bpoly, clr, opacs[i])

			#self.atlas.mark(-3.35, allps, clr)
#			self.atlas.mark(-3.35, [(cell['anteroposterior'], cell['flattened']) for cell in br['rabies']], clr)
			#self.atlas.mark(-3.35, [(z['anteroposterior'], z['flattened']) for z in br['sulcus']], 'red')

		#self.atlas.write(-3.35, path)
		self.atlas.group(-9.99)

	def draw_injection_sites(self):
		for br in self.d:
			inj = br['injection']
			clr = self._color(br['injection'], br['sulcus'])
			self.atlas.circle(-9.99, (inj['anteroposterior'], inj['flattened']), color=clr, ring=True)
		self.atlas.group(-9.99)

	def draw_inj_sites(self):
		for br in self.d:


				ps = []
				for cell in br['rabies']:
					if cell['anteroposterior'] < -2.3 or cell['flattened'] < 4.0 or cell['flattened'] > 5.2:
						ps.append((cell['anteroposterior'], cell['flattened']))

	def draw_squares(self, path='flatmap-squares.svg'):
		leg = np.mean([br['slice-width'] for br in self.d])
		zs = [c['anteroposterior'] for br in self.d for c in br['rabies']]
		minz = min(zs)
		zrange = (min(zs), max(zs))
		print '\tZs range between %.2f and %.2f' % (zrange[0], zrange[1])

		tops, bots, sulc = [], [], []

		for br in self.d:
			wrongminz, intzs = self.integerize(br, leg, minz)
			localminz = min([c['anteroposterior'] for c in br['rabies']])
			offset = int(math.floor((localminz - zrange[0])/leg))
			for c in br['rabies']:
				z = intzs[c['anteroposterior']] + offset
				while len(tops) < z + 1:
					tops.append(None)
					bots.append(None)
				if tops[z] == None or c['flattened'] < tops[z]:
					tops[z] = c['flattened']
				if bots[z] == None or c['flattened'] > bots[z]:
					bots[z] = c['flattened']

			for m in br['sulcus']:
				if m['anteroposterior'] >= zrange[0] and m['anteroposterior'] <= zrange[1]:
					z = intzs[m['anteroposterior']] + offset
					while len(sulc) < z + 1: sulc.append([])
					sulc[z].append(m['flattened'])

		# Take the means of the different measured sulci for each brain and interpolate
		# over missing measurements (if there are any)
		sulc = np.array([np.mean(s) for s in sulc])
		sulc[np.isnan(sulc)] = np.interp(np.flatnonzero(np.isnan(sulc)), np.flatnonzero(~np.isnan(sulc)), sulc[~np.isnan(sulc)])

		nystop = [int(math.ceil((sulc[i] - tops[i])/leg)) if tops[i] != None and tops[i] < sulc[i] else 0 for i in range(len(tops))]
		nysbot = [int(math.ceil((bots[i] - sulc[i])/leg)) if bots[i] != None and bots[i] > sulc[i] else 0 for i in range(len(tops))]

		tops = [sulc[i] - nystop[i]*leg for i in range(len(tops))]
		nys = [nystop[i] + nysbot[i] for i in range(len(tops))]
		print '\tYs range between %.2f and %.2f' % (min(tops), max([tops[i] + nys[i]*leg for i in range(len(tops))]))

		out = [[[0 for j in range(int(nys[i]))] for i in range(len(nys))] for i in self.d]
		maxes = [0 for i in self.d]
		clrs = []

#		sulc = savitzky_golay(sulc, 21, 3)

#		print tops
#		print sulc
#		print nys

		for i, br in enumerate(self.d):
			clr = self._color(br['injection'], br['sulcus'])

			print 'Brain %15s is colored %s' % (br['name'], clr)
			clrs.append(clr)
			localminz = min([c['anteroposterior'] for c in br['rabies']])
			wrongminz, intzs = self.integerize(br, leg, minz)
			offset = int(math.floor((localminz - zrange[0])/leg))

			for c in br['rabies']:
				z = intzs[c['anteroposterior']] + offset
				y = int(math.floor((c['flattened'] - tops[z])/leg))
				out[i][z][y] += 1
				if out[i][z][y] > maxes[i]:
					maxes[i] = out[i][z][y]

		for i in range(len(nys)):
			l = zrange[0] + i*leg
			for j in range(int(nys[i])):
				#t = tops[i] + j*leg
				t = sulc[i] - nystop[i]*leg + j*leg
				weights = [float(out[c][i][j])/maxes[c] if maxes[c] > 0 else 0 for c in range(len(clrs))]
				clr = self._weight_av_colors(clrs, weights)
				self.atlas.rectangle(-9.99, (l, t), (l + leg, t + leg), clr, 1.0)
			self.atlas.line(-9.99, (l, sulc[i]), (l + leg, sulc[i]), 'red')

		self.atlas.group(-9.99)

	def _color(self, inj, sulcus):
		aprange, flatrange = self._injection_ranges()
		sulcdist = self._dist_sulcus(inj, sulcus)
		ap = (inj['anteroposterior'] - min(aprange))/(max(aprange) - min(aprange))
		flat = (sulcdist - min(flatrange))/(max(flatrange) - min(flatrange))
		return self._colorfield(ap, flat)

	def _colormap(self, pos, colors):
		cpos = [i*(1.0/(len(colors) - 1)) for i in range(len(colors))]
		i = 0
		while pos > cpos[i + 1]: i += 1
		weight1 = 1 - (pos - cpos[i])/(1.0/(len(colors) - 1))
		return self._weight_av_colors([colors[i], colors[i + 1]], [weight1, 1 - weight1])

	def subtract_furthest_from_com(self, ps):
		cx, cy = np.average([p[0] for p in ps]), np.average([p[1] for p in ps])
		maxd, maxi = -1, -1
		for i, p in enumerate(ps):
			d = math.hypot(p[0] - cx, p[1] - cy)
			if d > maxd:
				maxd = d
				maxi = i
		return [p for i, p in enumerate(ps) if i != maxi]

	def bounding_poly(self, ps):
		from scipy.spatial import ConvexHull as ConvexHull
		ps = np.array(ps)
		hull = ConvexHull(ps, qhull_options='QJ')
		xs = ps[hull.vertices, 0]
		ys = ps[hull.vertices, 1]
		return zip(xs, ys)

	def _colorfield(self, horz, vert):
		light_colors = [
			'#27AAE1', # Blues
			'#F26BBF',  # Pink/Red
			'#F7941E',  # Orange
			'#8DC63F', # Greens
		]

		dark_colors = [
			'#1C75BC',
			'#ED1C24',
			'#F15A29',
			'#009444',
		]

		dark_colors = [
			'#1C75BC',
			'#ED1C24',
			'#F1D729', # this is yellow, orange is '#F15A29',
		]

		light = self._colormap(vert, light_colors)
		dark = self._colormap(vert, dark_colors)
		color = self._weight_av_colors([light, dark], [1.0 - horz, horz])
		return dark

	def _weight_av_colors(self, clrs, weights):
		if len(clrs) == 0 or np.sum(weights) == 0: return '#FFFFFF'
		colors = [self._hex_to_rgb(cl) for cl in clrs] + [(255, 255, 255)]
		weights.append(1 - sum(weights))

		r = int(math.floor(np.sum([colors[i][0]*weights[i] for i in range(len(colors))])))
		g = int(math.floor(np.sum([colors[i][1]*weights[i] for i in range(len(colors))])))
		b = int(math.floor(np.sum([colors[i][2]*weights[i] for i in range(len(colors))])))

		return self._rgb_to_hex((r, g, b))

	def _waverage_tuples(self, tuples, howmuchof0):
		return tuple([(tuples[0][i] - tuples[1][i])*howmuchof0 + tuples[1][i] for i in range(len(tuples[0]))])

	def _average_colors(self, listofcolors):
		if len(listofcolors) == 0: return '#FFFFFF'
		r = int(math.floor(np.mean([self._hex_to_rgb(cl)[0] for cl in listofcolors])))
		g = int(math.floor(np.mean([self._hex_to_rgb(cl)[1] for cl in listofcolors])))
		b = int(math.floor(np.mean([self._hex_to_rgb(cl)[2] for cl in listofcolors])))
		return self._rgb_to_hex((r, g, b))


	def _hex_to_rgb(self, val):
		val = val.lstrip('#')
		lv = len(val)
		return tuple(int(val[i:i + lv//3], 16) for i in range(0, lv, lv//3))

	def _rgb_to_hex(self, val):
		return '#%02x%02x%02x'.upper() % val


# http://wiki.scipy.org/Cookbook/SavitzkyGolay
def savitzky_golay(y, window_size, order, deriv=0, rate=1):
    r"""Smooth (and optionally differentiate) data with a Savitzky-Golay filter.
    The Savitzky-Golay filter removes high frequency noise from data.
    It has the advantage of preserving the original shape and
    features of the signal better than other types of filtering
    approaches, such as moving averages techniques.
    Parameters
    ----------
    y : array_like, shape (N,)
        the values of the time history of the signal.
    window_size : int
        the length of the window. Must be an odd integer number.
    order : int
        the order of the polynomial used in the filtering.
        Must be less then `window_size` - 1.
    deriv: int
        the order of the derivative to compute (default = 0 means only smoothing)
    Returns
    -------
    ys : ndarray, shape (N)
        the smoothed signal (or it's n-th derivative).
    Notes
    -----
    The Savitzky-Golay is a type of low-pass filter, particularly
    suited for smoothing noisy data. The main idea behind this
    approach is to make for each point a least-square fit with a
    polynomial of high order over a odd-sized window centered at
    the point.
    Examples
    --------
    t = np.linspace(-4, 4, 500)
    y = np.exp( -t**2 ) + np.random.normal(0, 0.05, t.shape)
    ysg = savitzky_golay(y, window_size=31, order=4)
    import matplotlib.pyplot as plt
    plt.plot(t, y, label='Noisy signal')
    plt.plot(t, np.exp(-t**2), 'k', lw=1.5, label='Original signal')
    plt.plot(t, ysg, 'r', label='Filtered signal')
    plt.legend()
    plt.show()
    References
    ----------
    .. [1] A. Savitzky, M. J. E. Golay, Smoothing and Differentiation of
       Data by Simplified Least Squares Procedures. Analytical
       Chemistry, 1964, 36 (8), pp 1627-1639.
    .. [2] Numerical Recipes 3rd Edition: The Art of Scientific Computing
       W.H. Press, S.A. Teukolsky, W.T. Vetterling, B.P. Flannery
       Cambridge University Press ISBN-13: 9780521880688
    """
    import numpy as np
    from math import factorial

    try:
        window_size = np.abs(np.int(window_size))
        order = np.abs(np.int(order))
    except ValueError, msg:
        raise ValueError("window_size and order have to be of type int")
    if window_size % 2 != 1 or window_size < 1:
        raise TypeError("window_size size must be a positive odd number")
    if window_size < order + 2:
        raise TypeError("window_size is too small for the polynomials order")
    order_range = range(order+1)
    half_window = (window_size -1) // 2
    # precompute coefficients
    b = np.mat([[k**i for i in order_range] for k in range(-half_window, half_window+1)])
    m = np.linalg.pinv(b).A[deriv] * rate**deriv * factorial(deriv)
    # pad the signal at the extremes with
    # values taken from the signal itself
    firstvals = y[0] - np.abs( y[1:half_window+1][::-1] - y[0] )
    lastvals = y[-1] + np.abs(y[-half_window-1:-1][::-1] - y[-1])
    y = np.concatenate((firstvals, y, lastvals))
    return np.convolve( m[::-1], y, mode='valid')

if __name__ == '__main__':
	hm = HeatMapper(test=True)
	hm.draw_squares()
	hm.draw_bounding_polys()
	hm.draw_cells()
	hm.draw_injection_sites()
	hm.render()

	#if len(argv) > 1:

	#	cr = CorticalRabies(argv[1])
	#	cr.flatmap()
		#print cr.coords({'marker':7})
		#cr.draw_gradientfield(-3.52)

		#cr = CorticalRabies(argv[1])
		#cr.test(-3.52)


	#else:
	#	print '\tERROR: please ennter path to cell counter file as argument'
