import math
import os, os.path as opath
import re
import xml.etree.ElementTree as ET

from svg_components.svgpath import CubicBezier
from svg_components.svgparser import parse_path


# Initialize AtlasPoint with a data type from rwAtlas and the translation to/from SVG
# coordinates
class AtlasPoint:
	def __init__(self, data, coords_to_svg):
		self.d = data
		self.trans = coords_to_svg

	# Return the center point of a circle, the top point of a line, or a vector from the
	# input point to the closest center of the circle or point along a line
	def xy(self, p1=False):
		if self.d['type'] == 'circle':
			out = self._svg_to_coords((self.d['x'], self.d['y']))
			if p1 == False: return out
			else: return p1 + out

		elif self.d['type'] == 'path':
			if p1 == False:
				out = self._cmplx(self.d['path'].point(0))
				return self._svg_to_coords(out)
			else:
				atlasp1 = self._coords_to_svg(p1)
				out = self._cmplx(self.d['path'].closest_point(atlasp1))
				out = self._svg_to_coords(out)
				return p1 + out

	# Return x,y coordinates relative to a flattened line. This is especially useful
	# for unfolding the cortex
	def flatxy(self, p1):
		atlasp1 = self._coords_to_svg(p1)
		ln, outp = self.d['path'].closest_point(atlasp1, flattened=True)
		if self._cmplx(self.d['path'].point(0))[1] > self._cmplx(self.d['path'].point(1))[1]:
			ln = 1 - ln
		outp = self._svg_to_coords(self._cmplx(outp))
		x = ln*self.d['path'].length()/self.trans[0]
		y = math.sqrt((p1[0] - outp[0])*(p1[0] - outp[0]) + (p1[1] - outp[1])*(p1[1] - outp[1]))
		return (x, y)

	# Return the length of a path
	def length(self):
		if self.d['type'] != 'path': return False
		else: return self.d['path'].length()/self.trans[0]

	# Convert the bregma x,y coordinates into svg x,y coordinates
	def _coords_to_svg(self, p1):
		return (p1[0]*self.trans[0] + self.trans[1], p1[1]*self.trans[2] + self.trans[3])

	# Convert the svg x,y coordinates into bregma x,y coordinates
	def _svg_to_coords(self, p1):
		return ((p1[0] - self.trans[1])/self.trans[0], (p1[1] - self.trans[3])/self.trans[2])

	# Convert a complex number (the way the svg library represents points) to a tuple
	# (the way we represent points)
	def _cmplx(self, p1): return (p1.real, p1.imag)

# Read from and write to atlas files
class AtlasReader:
	def __init__(self, dir):
		fs = [opath.join(dir, f) for f in os.listdir(dir)]
		self.writenum = 9
		self.read(fs)

	def read(self, fs):
		fimagenames = [(self._isatlas(f), f) for f in fs]
		self.data = {}
		self.svgs = {}
		self.writeables = {}
		for f in fimagenames:
			if f[0] > -90:
				self.svgs[f[0]], self.data[f[0]], self.writeables[f[0]] = self._parse_svg(f[1])

	def mark(self, ap, ps, color='#5E5AE6', ring=False):
		ap = self._get_ap(ap)
		el = self.writeables[ap]
		color = self._color(color)
		for p in ps:
			newp = self._coords_to_svg(ap, p)
			if not ring: ET.SubElement(el, 'circle', {'fill':color, 'stroke':'none', 'r':'3', 'cx':str(newp[0]), 'cy':str(newp[1]), 'opacity':'0.7'})
			else: ET.SubElement(el, 'circle', {'stroke':color, 'fill':'none', 'r':'3', 'cx':str(newp[0]), 'cy':str(newp[1]), 'opacity': '0.7'})

	def circle(self, ap, c, radius=5, color='#000000', ring=True):
		ap = self._get_ap(ap)
		el = self.writeables[ap]
		color = self._color(color)
		newc = self._coords_to_svg(ap, c)

		if not ring:
			ET.SubElement(el, 'circle', {'fill':color, 'stroke':'none', 'r':str(radius), 'cx':str(newc[0]), 'cy':str(newc[1]), 'opacity':'0.7'})
		else: ET.SubElement(el, 'circle', {'stroke':color, 'fill':'none', 'r':str(radius), 'cx':str(newc[0]), 'cy':str(newc[1]), 'opacity': '0.7'})

	def line(self, ap, p1, p2, color='black'):
		ap = self._get_ap(ap)
		el = self.writeables[ap]
		color = self._color(color)
		p1, p2 = self._coords_to_svg(ap, p1), self._coords_to_svg(ap, p2)
		ET.SubElement(el, 'line', {'stroke':color, 'stroke-width':'2', 'x1':str(p1[0]), 'y1':str(p1[1]), 'x2':str(p2[0]), 'y2':str(p2[1])})

	# Draw arrow from p1 to p2 with arrowhead at p2
	def arrow(self, ap, p1, p2, color='black'):
		if not hasattr(self, '_arrows_added'):
			self._arrows_added = {key:False for key in self.svgs}
		ap = self._get_ap(ap)
		el = self.writeables[ap]
		color = self._color(color)
		p1, p2 = self._coords_to_svg(ap, p1), self._coords_to_svg(ap, p2)

		if not self._arrows_added[ap]:
			self._arrows_added[ap] = True
			mark = ET.SubElement(el, 'marker', {'id':'arrowhead', 'viewBox':'0 0 10 10', 'refX':'0', 'refY':'5', 'markerUnits':'strokeWidth', 'markerWidth':'4', 'markerHeight':'3', 'orient':'auto'})
			ET.SubElement(mark, 'path', {'d':'M 0 0 L 10 5 L 0 10 z'})

		ET.SubElement(el, 'line', {'stroke':color, 'stroke-width':'1', 'marker-end':'url(#arrowhead)', 'x1':str(p1[0]), 'y1':str(p1[1]), 'x2':str(p2[0]), 'y2':str(p2[1])})

	def rectangle(self, ap, topleft, botright, color='none', fillopacity=1.0):
		ap = self._get_ap(ap)
		el = self.writeables[ap]
		color = self._color(color)
		topleft = self._coords_to_svg(ap, topleft)
		botright = self._coords_to_svg(ap, botright)
		ET.SubElement(el, 'rect', {'stroke':'black', 'stroke-width':'1', 'fill':color, 'fill-opacity': str(fillopacity), 'x':str(topleft[0]), 'y':str(topleft[1]), 'width':str(botright[0] - topleft[0]), 'height':str(botright[1] - topleft[1])})

	def polygon(self, ap, ps, color='#000000', opacity=0.1, closed=True):
		ap = self._get_ap(ap)
		el = self.writeables[ap]
		color = self._color(color)
		path = [self._coords_to_svg(ap, p) for p in ps]
		path = ' '.join([str(p[0]) + ',' + str(p[1]) for p in path])
		ET.SubElement(el, 'polygon', {'stroke':'none', 'fill':color, 'opacity': str(opacity), 'points':path})

	def clear(self, ap):
		ap = self._get_ap(ap)
		ol = self.writeables[ap]
		root = self.svgs[ap].getroot()
		root.remove(ol)

		self.writenum += 1
		el = ET.SubElement(root, 'g', {'id':'Layer_'+str(self.writenum)})
		self.writeables[ap] = el

	def group(self, ap):
		ap = self._get_ap(ap)
		root = self.svgs[ap].getroot()

		self.writenum += 1
		el = ET.SubElement(root, 'g', {'id':'Layer_'+str(self.writenum)})
		self.writeables[ap] = el

	def write(self, ap, savepath='temp.svg'):
		ap = self._get_ap(ap)
		tree = self.svgs[ap]
		tree.write(savepath)

	# Get all atlas data points as AtlasPoints, separated by color
	def get(self, ap):
		ap = self._get_ap(ap)
		self._get_translations(ap)
		tr = self._to_svg_translation[ap]

		colors = list(set([c['color'] for c in self.data[ap]]))
		out = {}
		for c in colors:
			out[c] = sorted([AtlasPoint(d, tr) for d in self.data[ap] if d['color'] == c], key=lambda apt: apt.xy()[1])
		return out

	# Add atlas colors here!
	def _atlascolor(self, name):
		colors = {
			'#EB292A': 'red',
			'#EA2C2D': 'red',
			'#ED1F24': 'red',
			'#6DBE47': 'green',
			'#70BF47': 'green',
			'#69BD45': 'green',
			'#71CDDD': 'cyan',
			'#73CDDD': 'cyan',
			'#6FCCDD': 'cyan',
			'#F4EA14': 'yellow',
			'#F4EA0F': 'yellow',
			'#F6EC13': 'yellow',
			'#F49921': 'orange',
			'#F49A23': 'orange',
		}

		if name not in colors:
			print '\tWARNING: %s not in atlas colors' % (name)
		else:
			return colors[name]

	def _color(self, name='indigo'):
		from re import match as rematch

		colors = {
			'orange': '#E86E0A',
			'red': '#D61E21',
			'gray': '#7C7C7C',
			'black': '#000000',
			'green': '#75D977',
			'mint': '#47D1A8',
			'purple': '#C880D1',
			'indigo': '#5E5AE6',
			'blue': '#47AEED', # previously 4087DD
			'yellow': '#F2E205',
			'cyan': '#71CDDD',
			'none': 'none',
		}

		if isinstance(name, int):
			clrs = [colors[key] for key in colors]
			return clrs[name%len(clrs)]
		elif name in colors: return colors[name]
		elif rematch('^(#[0-9A-Fa-f]{6})|(rgb(a){0,1}\([0-9,]+\))$', name): return name
		else: return colors['gray']

	def _get_translations(self, ap):
		if hasattr(self, '_to_svg_translation'):
			if ap in self._to_svg_translation:
				return True
		else:
			self._to_svg_translation = {}

		# Key translation is made from bregma, marked with a red point, and a second
		# red point 6 mm ventral
		reds = sorted([(p['x'], p['y']) for p in self.data[ap] if p['color'] == 'red'], key=lambda tup: tup[1])
		pix_per_mm = (reds[1][1] - reds[0][1])/6.0
		self._to_svg_translation[ap] = (pix_per_mm, reds[0][0], pix_per_mm, reds[0][1])

	def _coords_to_svg(self, ap, p1):
		self._get_translations(ap)

		tr = self._to_svg_translation[ap]
		return (p1[0]*tr[0] + tr[1], p1[1]*tr[2] + tr[3])

	def _svg_to_coords(self, ap, p1):
		self._get_translations(ap)

		tr = self._to_svg_translation[ap]
		return ((p1[0] - tr[1])/tr[0], (p1[1] - tr[3])/tr[2])

	# Return the closest anterior-posterior position from the atlas
	def _get_ap(self, z):
		zs = [key for key in self.data]
		ds = [abs(z - key) for key in zs]
		return zs[ds.index(min(ds))]

	# Check if a path is an atlas file
	def _isatlas(self, name):
		if re.match("^.*paxinos[-+]\d\.\d\d\.svg$", name):
			return float(name[name.rindex('paxinos') + 7:name.rindex('.')])
		else:
			return -99.0

	# Return SVG root, parsed registration data from SVG, and writeable element
	def _parse_svg(self, path):
		ET.register_namespace('', 'http://www.w3.org/2000/svg')
		tree = ET.parse(path)
		root = tree.getroot()
		parsedroot = self._reparse(root)

		el = ET.SubElement(root, 'g', {'id':'Layer_'+str(self.writenum)})
		return tree, parsedroot, el

	# Recursive parsing function
	def _reparse(self, el):
		out = []
		if 'fill' in el.attrib or 'stroke' in el.attrib:
			if 'fill' not in el.attrib or (el.attrib['fill'] != '#000000' and el.attrib['fill'] != '#FFFFFF'):
				if 'stroke' not in el.attrib or (el.attrib['stroke'] != '#000000' and el.attrib['stroke'] != '#FFFFFF'):
					tg = el.tag.replace('{http://www.w3.org/2000/svg}', '')
					if tg == 'circle':
						out.append({'type':'circle', 'color':self._atlascolor(el.attrib['fill']), 'x': float(el.attrib['cx']), 'y': float(el.attrib['cy'])})
					elif tg == 'path':
						out.append({'type':'path', 'color':self._atlascolor(el.attrib['stroke']), 'path': parse_path(el.attrib['d'])})
		else:
			for child in el:
				out.extend(self._reparse(child))
		return out
