import math
# ElementTree is a mechanism to parse XML files.
# Ref: https://docs.python.org/2/library/xml.etree.elementtree.html
import xml.etree.ElementTree as ET

# Class CellCounterFile is designed to read in files saved from FIJI's CellCounter
# module. It returns coordinates of clicked points in a list of dicts, each containing
# x, y, the z given to the class, and the marker type
class CellCounterFile():
	# If you want to run code on the instantiation of a class, use the __init__ function
	def __init__(self, path, z=-1):
		# Parse the XML file, see ref above
		tree = ET.parse(path)
		self.root = tree.getroot()

		# Set the z level from input
		self.z = z

		# Get the image name and all points from the file
		self.image_filename = self.get_unique_tag('Image_Filename').text
		marker_data = self.get_unique_tag('Marker_Data')
		self.ps = self.get_markers(marker_data)

		# Use the image series number to convert to millimeters
		self.convert_to_millimeters()

	# Get an xml element by unique tag name
	def get_unique_tag(self, name, el=None):
		# Begin with the root element
		if el == None:
			el = self.root

		# Initiate with no output
		out = None

		# Iterate through all child elements, returning if a match is found, recursing
		# if a match is not found
		for child in el:
			if child.tag == name:
				return child
			else:
				tag = self.get_unique_tag(name, child)
				if tag != None:
					out = tag

		return out

	# Get all marker points given the container element
	def get_markers(self, el):
		out = []

		# Uses element finding code built into ElementTree rather than traversing it by
		# hand as in get_unique_tag
		for mt in el.findall('Marker_Type'):
			mtype = int(mt.find('Type').text)

			# Find each marker, convert to ints, and save
			for pos in mt.findall('Marker'):
				x = int(pos.find('MarkerX').text)
				y = int(pos.find('MarkerY').text)
				out.append({'x': x, 'y': y, 'z': self.z, 'marker':mtype})

		return out

	def convert_to_millimeters(self):
		 # Pixel size from Sam's Matlab code in um
		vsi_pixel_size = 0.64497

		# Multiply the pixel size by 2 to the power of the series
		if self.image_filename.strip()[-2] == '#':
			vsi_pixel_size *= math.pow(2, int(self.image_filename[-1]) - 1)

		# Convert microns to millimeters
		vsi_pixel_size = vsi_pixel_size/1000.0

		# Iterate through all points and correct x and y
		for i in range(len(self.ps)):
			self.ps[i]['x'] = self.ps[i]['x']*vsi_pixel_size
			self.ps[i]['y'] = self.ps[i]['y']*vsi_pixel_size

	# Return the list of dicts of marked points
	# Ex. [{'x':, 'y':, 'z':, 'marker':}]
	def get(self):
		return self.ps


def cellcounter_file(path, z=-1):
	ccf = CellCounterFile(path, z)
	return ccf

if __name__ == '__main__':
	cf = CellCounterFile('/Users/arthur/Documents/Programs/CellWarp/k139-bla-lm/cellcounter_image_03.xml')
	print cf.get()
	print cf.z
